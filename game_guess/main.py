"""游戏的入口模块"""

from mygame import *
import sys


if __name__ == '__main__':
    if sys.argv[1] == 'start':
        sc()
        run()  # 游戏运行

    if sys.argv[1] == 'update':
        str2 = "0" * len(dict_key)
        f1 = open("./player_answers.txt", "w")
        f1.write(str2)  # 更新信息
        f1.close()


